# Fork

This is my personal fork of the vim editor written by Bram Moolenaar. This
sounds more fancy than it actually is: it's just a copy of vim with a build
script to compile and install vim to my local machine with Python support. I do
this so that I am more disto-agnostic, as well as (for some reason) noting a
performance boost when compiling from source.

To install simply call

`bash build.sh`

There are some notes on what dependencies might be needed in different systems
in `build.sh`.
