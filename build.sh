#!/bin/bash

# Exit on erros
set -e

# Make sure dependencies are installed
# Arch Linux:
# ncurses lib32-ncurses python python2
# Void Linux:
# ncurses ncurses-base ncurses-devel ncurses-libs ncurses-term python3 python
# Ubuntu:
# libncurses5 libncurses5-dev libncursesw5 ncurses-base ncurses-bin ncurses-term python3 python

cd vim || exit

make clean
./configure                 \
	--prefix="$HOME/.local" \
	--enable-pythoninterp   \
	--enable-python3interp

jobs=$(($(nproc)-1))
make -j $jobs -l 80 DESTDIR="$HOME/.local"
make install -j $jobs -l 80
